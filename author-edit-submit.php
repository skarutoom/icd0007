<?php
include_once __DIR__ . '/AuthorDao.php';
include_once __DIR__ . '/Author.php';
include_once 'tpl.php';

$authordao = new AuthorDao();
$firstName = htmlspecialchars($_POST['firstName'])  ?? '';
$lastName = htmlspecialchars($_POST['lastName']) ?? '';
$grade = $_POST['grade'] ?? '';
$id = $_POST['id'] ?? '';

$pageid = 'author-form-page';
$pageid1 = 'author-list-page';

$author = $authordao->getAuthorById($id);


$newauthor = new Author($firstName, $lastName, $grade);
$newauthor->id = $id;

if (isset($_POST['submitButton'])) {
    if (strlen($firstName) < 1 or strlen($firstName) > 21 or strlen($lastName) < 2 or strlen($lastName) > 22) {
        $message = 'Eesnimi peab olema 1 kuni 21 tähemärki ja perekonnanimi peab olema 2 kuni 22 tähemärki!';
        $data = [
            'pageid' => $pageid,
            'firstName' => $firstName,
            'lastName' => $lastName,
            'grade' => $grade,
            'id' => $id,
            'error' => $message,
            'template' => 'author-edit.html'
        ];
        print renderTemplate('tpl/main.html', $data);
    } else {

        $authordao->editAuthor($newauthor);
        $authors = $authordao->getAuthors();
        header("Location: ?cmd=author-list&message=updated");
        die();
    }
}

if (isset($_POST['deleteButton'])) {
    $authordao->deleteAuthorById($id);
    header("Location: ?cmd=author-list&message=deleted");
    die();
}

