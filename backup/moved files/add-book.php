<?php

require_once 'connection.php';

include_once __DIR__ . '/Book.php';
include_once __DIR__ . '/BookDao.php';
include_once 'tpl.php';


$title = $_POST['title'] ?? '';
$grade = $_POST['grade'] ?? 0;
$temp_isRead = $_POST['isRead'] ?? '';
$id = $_POST['id'] ?? '';
$author1 = $_POST['author1'] ?? '';
$author2 = $_POST['author2'] ?? '';

if ($temp_isRead === 'Yes') {
    $isRead = 1;
} else {
    $isRead = 0;
}

$book = new Book($title, $grade, $isRead, $author1, $author2);
$book->id = $id;

$bookdao = new BookDao();

if (strlen($title) < 3 or strlen($title) > 23 ) {
    $message = "Pealkiri peab olema 3-23";
    $data = [
        'title' => $title,
        'grade' => $grade,
        'isread' => $isRead,
        'author1' => $author1,
        'author2' => $author2,
        'error' => $message,
        'template' => 'book-add.html'

    ]; print renderTemplate('tpl/main.html', $data);
    die();
}

if (isset($_POST['deleteButton'])) {
    $bookdao->deleteBookById($id);
    require_once 'book-list.php';
}

if (isset($_POST['submitButton'])) {
    if ($_POST['submitButton'] === 'Uuenda') {
        $bookdao->editBook($book);
        require_once 'book-list.php';
    }
    if ($_POST['submitButton'] === 'Salvesta') {
        $bookdao->saveBook($book);
        require_once 'book-list.php';
    }
}