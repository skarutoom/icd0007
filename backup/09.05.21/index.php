<!DOCTYPE html>
<html lang="et">
<head>
    <meta charset="UTF-8">
    <title>Raamatute nimekiri</title>
    <link href="styles.css" rel="stylesheet">
</head>
<body id="book-list-page">
<nav>
    <table>
        <tr>
            <td>
                <a href="index.php" id="book-list-link">Raamatud</a> |
                <a href="book-add.php" id="book-form-link">Lisa raamat</a> |
                <a href="author-list.php" id="author-list-link">Autorid</a> |
                <a href="author-add.php" id="author-form-link">Lisa autor</a>
            </td>
        </tr>
    </table>
</nav>
<?php
$return_m = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";

if (strpos($return_m, "title_error") == true) {
    print('<div id="error-block">Pealkiri peab olema 3 kuni 23 tähemärki!</div>');
}

if (strpos($return_m, "saved") == true) {
    print('<div id="message-block">Salvestatud!</div>');
}
if (strpos($return_m, "deleted") == true) {
    print('<div id="message-block">Kustutatud!</div>');
}
if (strpos($return_m, "updated") == true) {
    print('<div id="message-block">Uuendatud!</div>');
}
?>
<br>
<table>
    <tr>
        <td>Pealkiri</td>
        <td>Autorid</td>
        <td>Hinne</td>
    </tr>
    <tr>
        <td colspan="3"><hr></td>
    </tr>
    <?php
    require_once 'connection.php';
    $conn = getConnection();

    $stmt = $conn->prepare('SELECT * FROM books LEFT JOIN books_authors ON books_authors.bookId = books.id LEFT JOIN authors ON books_authors.authorId = authors.id');

    $stmt->execute();
    $authors = [];
    foreach ($stmt as $row) {
        //print "-----1-----" . PHP_EOL;
        $bookId = $row['bookId'];
        $title = $row['title'];
        $author = $row['firstName'] . ' ' . $row['lastName'];
        $grade = $row['grade'];
        $isRead = $row['isRead'];
        if (isset($authors[$bookId])) {

            $authors[$bookId][3] = $author;
            $authors[$bookId][4] = $grade;
            $authors[$bookId][5] = $isRead;

        } else {
            $authors[$bookId] = [$bookId, $title, $author, ' ', $grade, $isRead];
        }
    }

    foreach ($authors as $book) {
//        [$id, $title, $grade, $isRead] = explode(';', trim($book));
        echo '<tr>';
        echo '<td>' . '<a href="book-edit.php?id=' . $book[0]. '">' . urldecode($book[1]) . '</a>' . '</td>' . PHP_EOL;
        echo '<td>' .  $book[2] . ' ' . $book[3] . '</td>';
       echo '<td>' . str_repeat('★', (int)$book[4]) . '</td>';
        echo '</tr>';

    }?>
</table>
<br>
<br>
<br>
<br>
<footer>
    ICD0007 Harjutus
</footer>
</body>
</html>