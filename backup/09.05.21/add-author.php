<?php

require_once 'connection.php';

include_once __DIR__ . '/Author.php';


$firstName = $_POST['firstName'] ?? '';
$lastName = $_POST['lastName'] ?? '';
$grade = $_POST['grade'] ?? 0;
$id = $_POST['id'] ?? '';

$author = new Author($firstName, $lastName, $grade);
$author->id = $id;

if (strlen($firstName) < 1 or strlen($firstName) > 21 or strlen($lastName) < 2 or strlen($lastName) > 22) {
    header("Location: author-add.php?name_error&firstname=$firstName&lastname=$lastName&grade=$grade");
    die();
}

if (isset($_POST['deleteButton'])) {
    deleteAuthorById($id);
    header("Location: author-list.php?deleted");
    die();
}

function saveAuthor(Author $author) {
    $conn = getConnection();

    $stmt = $conn->prepare(
        'INSERT INTO authors (firstName, lastName, authorgrade) VALUES (:firstname, :lastname, :grade);');

    $stmt->bindValue(':firstname', htmlspecialchars($author->firstName, ENT_QUOTES));
    $stmt->bindValue(':lastname', htmlspecialchars($author->lastName, ENT_QUOTES));
    $stmt->bindValue(':grade', $author->grade);

    $stmt->execute();

    return $conn->lastInsertId();
}

function editAuthor(Author $author) {
    $conn = getConnection();
    $stmt = $conn->prepare(
        'UPDATE authors set 
                 firstName = :firstname,
                 lastName = :lastname,
                 authorgrade = :grade
                 WHERE id=:id');
    $stmt->bindValue(':firstname', htmlspecialchars($author->firstName, ENT_QUOTES));
    $stmt->bindValue(':lastname', htmlspecialchars($author->lastName, ENT_QUOTES));
    $stmt->bindValue(':grade', $author->grade);
    $stmt->bindValue(':id', $author->id);
    $stmt->execute();
}

if (isset($_POST['submitButton'])) {
    if ($_POST['submitButton'] === 'Uuenda') {
        editAuthor($author);
        header("Location: author-list.php?updated");
        die();
    }
    if ($_POST['submitButton'] === 'Salvesta') {
        saveAuthor($author);
        header("Location: author-list.php?saved");
        die();
    }
}

function deleteAuthorById(string $id) : void {

    $conn = getConnection();
    $stmt = $conn->prepare(
        'DELETE FROM authors WHERE id=:id');
    $stmt->bindValue(':id', $id);
    $stmt->execute();
}

function getAllAuthors() : array
{

    $lines = file(DATA_FILE);

    $result = [];
    foreach ($lines as $line) {
        [$id, $firstName, $lastName, $grade] = explode(';', trim($line));

        $author = new Author(urldecode($firstName), urldecode($lastName), urlencode($grade));

        $author->id = $id;

        $result[] = $author;
    }

    return $result;
}

function getAuthorAsLine($author): string {
    return urlencode($author->id)
        . ';' . urlencode($author->firstName)
        . ';' . urlencode($author->lastName)
        . ';' . urlencode($author->grade) . PHP_EOL;
}

function getNewId() : string {
    $contents = file_get_contents(ID_FILE);

    $id = intval($contents);

    file_put_contents(ID_FILE, $id + 1);

    return strval($id);
}

?>