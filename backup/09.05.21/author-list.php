<!DOCTYPE html>
<html lang="et">
<head>
    <meta charset="UTF-8">
    <title>Autorite nimekiri</title>
    <link href="styles.css" rel="stylesheet">
</head>
<body id="author-list-page">
<nav>
    <table>
        <tr>
            <td>
                <a href="index.php" id="book-list-link">Raamatud</a> |
                <a href="book-add.php" id="book-form-link">Lisa raamat</a> |
                <a href="author-list.php" id="author-list-link">Autorid</a> |
                <a href="author-add.php" id="author-form-link">Lisa autor</a>
            </td>
        </tr>
    </table>
</nav>
<?php
$return_m = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";

if (strpos($return_m, "author_error") == true) {
    print('<div id="error-block">Pealkiri peab olema 3 kuni 23 tähemärki!</div>');
}

if (strpos($return_m, "saved") == true) {
    print('<div id="message-block">Salvestatud!</div>');
}
if (strpos($return_m, "deleted") == true) {
    print('<div id="message-block">Kustutatud!</div>');
}
if (strpos($return_m, "updated") == true) {
    print('<div id="message-block">Uuendatud!</div>');
}
?>
<br>
<br>
<table>
    <tr>
        <td>Nimi</td>
        <td>Perkeonnanimi</td>
        <td>Hinne</td>
    </tr>
    <tr>
        <td colspan="3">
            <hr></td>
    </tr>
    <?php
    require_once 'connection.php';
    $conn = getConnection();

    $stmt= $conn->prepare('SELECT * FROM authors;');
    $stmt->execute();

    foreach ($stmt as $row) {
        echo '<tr>';
        echo '<td>' . '<a href="author-edit.php?id=' . $row['id'] . '">' . urldecode($row['firstName']) . '</a>' . '</td>' . PHP_EOL;
        echo '<td>' . $row['lastName'] . '</td>';
        echo '<td>' . str_repeat('★', (int)$row['authorgrade']) . '</td>';
        echo '</tr>';
    }?>
</table>
<br>
<br>
<br>
<br>
<footer>
    ICD0007 Harjutus
</footer>
</body>
</html>