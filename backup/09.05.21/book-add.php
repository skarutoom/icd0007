<?php
$book_id = $_GET['id'] ?? '';
$title = $_GET['title'] ?? '';
$grade = $_GET['grade'] ?? '';
$actualGrade = $_GET['grade'] ?? '';
$isRead = $_GET['isread'] ?? '';

if ($isRead === '1') {
    $reading = 'checked';
}
?>

<!DOCTYPE html>
<html lang="et">
<head>
    <meta charset="UTF-8">
    <title>Lisa Raamat</title>
    <link href="styles.css" rel="stylesheet">
</head>
<body id="book-form-page">
<nav>
    <table>
        <tr>
            <td>
                <a href="index.php" id="book-list-link">Raamatud</a> |
                <a href="book-add.php" id="book-form-link">Lisa raamat</a> |
                <a href="author-list.php" id="author-list-link">Autorid</a> |
                <a href="author-add.php" id="author-form-link">Lisa autor</a>
            </td>
        </tr>
    </table>
</nav>
<?php
$return_m = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";

if (strpos($return_m, "title_error") == true) {
    print('<div id="error-block">Pealkiri peab olema 3 kuni 23 tähemärki!</div>');
}
?>
<br>
<br>
<form method="post" action="add-book.php">
    <input name="id" type="hidden" value="<?= $book_id ?>">

    <table>
        <tr>
            <td>
                <label for="title">Pealkiri:</label>
            </td>
            <td>
                <input type="text"
                       id="title"
                       name="title"
                       value="<?= $title ?>"

                >
            </td>
        </tr>
        <tr>
            <td>
                <label for="author1">Autor 1:</label>
            </td>
            <td>
                <select id="author1" name="author1">
                    <option value=""></option>

                </select></td>
        </tr>
        <tr>
            <td>
                <label for="author2">Autor 2:</label>
            </td>
            <td>
                <select id="author2" name="author2">
                    <option value=""></option>
                    <?php
                    require_once 'connection.php';

                    $conn = getConnection();

                    $stmt = $conn->prepare('SELECT * FROM authors');

                    $stmt->execute();

                    foreach ($stmt as $author) {
                        echo '<option value=' . $author['id'] . '>' . $author['firstName'] . ' ' . $author['lastName'] . '</option>';
                    }
                    ?>
            </select>
            </td>
        </tr>
        <tr>
            <td>
                <label>Hinne:</label>
            </td>
            <td>
                <?php foreach (range(1, 5) as $grade): ?>

                    <input type="radio"
                           name="grade"
                        <?= strval($grade) === $actualGrade ? 'checked' : ''; ?>
                           value="<?= $grade ?>" />
                    <?= $grade ?>

                <?php endforeach; ?>
            </td>
        </tr>
        <tr>
            <td>
                <label for="isRead">Loetud:</label>
            </td>
            <td>
                <input type="checkbox"
                       id="isRead"
                       name="isRead"
                       value="Yes"
                    <?php if ($isRead === '1') {
                        print 'checked';
                    }?>
                >
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <input type="submit" id="submitButton" name="submitButton" value="Salvesta">
            </td>
        </tr>
    </table>
</form>
<br>
<br>
<br>
<br>
<footer>
    ICD0007 Harjutus
</footer>
</body>
</html>