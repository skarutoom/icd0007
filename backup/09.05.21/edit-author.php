<?php


const DATA_FILE = __DIR__ . '/authors.txt';


$firstName = $_POST['firstName'] ?? '';
$lastName = $_POST['lastName'] ?? '';
$grade = $_POST['grade'] ?? '';

$line = urlencode($firstName) . ';' . urlencode($lastName) . ';' . urlencode($grade) . PHP_EOL;

file_put_contents(DATA_FILE, $line, FILE_APPEND);

if ($_POST['submitButton'] === 'Save') {
    header("Location: author-list.php");
}
?>
