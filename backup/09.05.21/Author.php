<?php


class Author {
    public string $id;
    public string $firstName;
    public string $lastName;
    public string $grade;

    public function __construct(string $firstName, string $lastName, string $grade) {
        $this->id = '';
        $this->firstName = $firstName;
        $this->lastName = $lastName;
        $this->grade = $grade;
    }

    public function __toString() : string {
        return sprintf(' Id: %s, First Name: %s, Last Name: %s, Grade: %s', $this->id, $this->firstName, $this->lastName, $this->grade);
    }
}
