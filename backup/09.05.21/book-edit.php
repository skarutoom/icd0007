<?php
$book_id = $_GET['id'] ?? '';

function getBookByID($id) : array {

    require_once 'connection.php';

    $conn = getConnection();

    $stmt = $conn->prepare('SELECT * FROM books 
            LEFT JOIN books_authors ON books_authors.bookId = books.id 
            LEFT JOIN authors ON books_authors.authorId = authors.id
            WHERE books.id = :id');
    $stmt->bindValue(':id', $id);
    $stmt->execute();

    $return_array = [];
    foreach ($stmt as $data) {
        $return_array['id'] = $data[0];
        $return_array['title'] = $data['title'];
        $return_array['grade'] = $data['grade'];
        $return_array['isread'] = $data['isRead'];
        $return_array['authors'][] = $data['id'];

    }return $return_array;
}

$data = getBookByID($book_id);

$author1 = $data['authors'][0];
$author2 = $data['authors'][1];

?>

<!DOCTYPE html>
<html lang="et">
<head>
    <meta charset="UTF-8">
    <title>Lisa Raamat</title>
    <link href="styles.css" rel="stylesheet">
</head>
<body id="book-form-page">
<nav>
    <table>
        <tr>
            <td>
                <a href="index.php" id="book-list-link">Raamatud</a> |
                <a href="book-add.php" id="book-form-link">Lisa raamat</a> |
                <a href="author-list.php" id="author-list-link">Autorid</a> |
                <a href="author-add.php" id="author-form-link">Lisa autor</a>
            </td>
        </tr>
    </table>
</nav>
<br>
<br>
<br>
<form method="post" action="add-book.php">
    <input name="id" type="hidden" value="<?php echo $data["id"];?>">

    <table>
        <tr>
            <td>
                <label for="title">Pealkiri:</label>
            </td>
            <td>
                <input type="text"
                       id="title"
                       name="title"
                       minlength="3" maxlength="23"
                       value="<?php echo $data["title"];?>"

                >
            </td>
        </tr>
        <tr>
            <td>
                <label for="author1">Autor 1:</label>
            </td>
            <td>
                <select id="author1" name="author1">
                    <option value="" ></option>

                </select></td>
        </tr>
        <tr>
            <td>
                <label for="author2">Autor 2:</label>
            </td>
            <td>
                <select id="author2" name="author2">
                    <option value="" ></option>

            </select>
            </td>
        </tr>
        <tr>
            <td>
                <label>Hinne:</label>
            </td>
            <td>

            </td>
        </tr>
        <tr>
            <td>
                <label for="isRead">Loetud:</label>
            </td>
            <td>
                <input type="checkbox"
                       id="isRead"
                       name="isRead"
                       value="Yes"
                >
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <input type="submit" id="deleteButton" name="deleteButton" value="Kustuta">
                <input type="submit" id="submitButton" name="submitButton" value="Uuenda">
            </td>
        </tr>
    </table>
</form>
<br>
<br>
<br>
<br>
<footer>
    ICD0007 Harjutus
</footer>
</body>
</html>