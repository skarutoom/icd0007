<?php

require_once 'connection.php';

include_once __DIR__ . '/Book.php';

$title = $_POST['title'] ?? '';
$grade = $_POST['grade'] ?? 0;
$temp_isRead = $_POST['isRead'] ?? '';
$id = $_POST['id'] ?? '';
$author1 = $_POST['author1'] ?? '';
$author2 = $_POST['author2'] ?? '';

if ($temp_isRead === 'Yes') {
    $isRead = 1;
} else {
    $isRead = 0;
}

$book = new Book($title, $grade, $isRead, $author1, $author2);
$book->id = $id;

if (strlen($title) < 3 or strlen($title) > 23 ) {
    header("Location: book-add.php?title_error&title=$title&grade=$grade&isread=$isRead");
    die;
}

if (isset($_POST['deleteButton'])) {
    deleteBookById($id);
    header("Location: index.php?deleted");
}

function saveBook(Book $book) {
    $conn = getConnection();
    $stmt = $conn->prepare(
        'INSERT INTO books (title, grade, isRead) VALUES (:title, :grade, :isRead);');

    $stmt->bindValue(':title', htmlspecialchars($book->title, ENT_QUOTES));
    $stmt->bindValue(':grade', $book->grade);
    $stmt->bindValue(':isRead', intval($book->isRead));

    $stmt->execute();
    $book->id = $conn->lastInsertId();

    if (isset($book->author1)) {
        bindBookAuthor($book->id, $book->author1);
    }
    if (isset($book->author2)) {
        bindBookAuthor($book->id, $book->author2);
    }

    return $book->id;
}

function editBook(Book $book) {
    $conn = getConnection();
    $stmt = $conn->prepare(
        'UPDATE books set 
                 title = :title,
                 grade = :grade,
                 isRead = :isRead
                 WHERE id=:id;');
    $stmt->bindValue(':title', htmlspecialchars($book->title, ENT_QUOTES));
    $stmt->bindValue(':grade', $book->grade);
    $stmt->bindValue(':isRead', $book->isRead);
    $stmt->bindValue(':id', $book->id);
    $stmt->execute();

    updateBookAuthor($book->id);

    if (isset($book->author1)) {
        bindBookAuthor($book->id, $book->author1);
    }
    if (isset($book->author2)) {
        bindBookAuthor($book->id, $book->author2);
    }

}

if (isset($_POST['submitButton'])) {
    if ($_POST['submitButton'] === 'Uuenda') {
        editBook($book);
        header("Location: index.php?updated");
    }
    if ($_POST['submitButton'] === 'Salvesta') {
        saveBook($book);
        header("Location: index.php?saved");
    }
}

function deleteBookById($id) {
    $conn = getConnection();
    $stmt = $conn->prepare(
        'DELETE FROM books WHERE id=:id;');
    $stmt->bindValue(':id', $id);
    $stmt->execute();
}

function bindBookAuthor($bookId, $authorId) {
    $conn = getConnection();
    $stmt = $conn->prepare(
        'INSERT INTO books_authors (bookId, authorId) VALUE (:bookid, :authorid);');
    $stmt->bindValue(':bookid', intval($bookId));
    $stmt->bindValue(':authorid',intval($authorId));
    $stmt->execute();
}

function updateBookAuthor($bookId) {
    $conn = getConnection();
    $stmt = $conn->prepare(
        'DELETE from books_authors WHERE bookId = :bookid');
    $stmt->bindValue(':bookid', intval($bookId));
    $stmt->execute();
}

function editBookAuthor($bookId, $authorId) {
    $conn = getConnection();
}

?>