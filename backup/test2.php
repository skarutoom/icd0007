<?php
$id = 2;
require_once 'connection.php';

$conn = getConnection();

$stmt = $conn->prepare('SELECT * FROM books
LEFT JOIN books_authors ON books_authors.bookId = books.id
LEFT JOIN authors ON books_authors.authorId = authors.id
WHERE books.id = :id');
$stmt->bindValue(':id', $id);
$stmt->execute();

//print_r($stmt->fetchAll());

$return_array = [];
foreach ($stmt as $data) {
    $return_array['bookid'] = $data['id'];
    $return_array['title'] = $data['title'];
    $return_array['grade'] = $data['grade'];
    $return_array['isread'] = $data['isRead'];
    $return_array['authors'][] = $data['id'];
}

$author1 = $return_array['authors'][0];
$author2 = $return_array['authors'][1];

print ($author1) . PHP_EOL;
print ($author2) . PHP_EOL;

?>