<?php

include_once __DIR__ . '/BookDao.php';
include_once __DIR__ . '/AuthorDao.php';
include_once 'tpl.php';
$id = $_GET['id'];
$bookdao = new BookDao();
$authordao = new AuthorDao();

$books = $bookdao->getBookByID($id);
$author1 = $books['authors'][0] ?? '';
$author2 = $books['authors'][1] ?? '';

$authors = $authordao->getAuthors();
$data = [
        "authors" => $authors,
        "title" => $books['title'],
        "author1" => $author1,
        "author2" => $author2,
        "grade" => $books['grade'],
        "id" => $books['id'],
        "isRead" => $books['isread'],
        "bookauthors" => $books['authors'],
        "template" => "book-edit.html"

];
print renderTemplate("tpl/main.html", $data);