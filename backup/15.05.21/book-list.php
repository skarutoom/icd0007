<?php

include_once __DIR__ . '/BookDao.php';
include_once 'tpl.php';

$url = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";

$message = $_POST['query'] ?? '';

if (strpos($url, "delete") == true) {
    $message = 'Kustutatud!';
}

if (strpos($url, "save") == true) {
    $message = 'Salvestatud!';
}

if (strpos($url, "update") == true) {
    $message = 'Uuendatud!';
}

$dao = new BookDao();
$books = $dao->getBooks();


$data = [
    'message' =>$message,
    'books' => $books,
    'template' => 'book-list.html'
];
print renderTemplate('tpl/main.html', $data);
