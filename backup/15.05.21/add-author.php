<?php

require_once 'connection.php';

include_once __DIR__ . '/Author.php';
include_once __DIR__ . '/AuthorDao.php';


$firstName = $_POST['firstName'] ?? '';
$lastName = $_POST['lastName'] ?? '';
$grade = $_POST['grade'] ?? 0;
$id = $_POST['id'] ?? '';

$author = new Author($firstName, $lastName, $grade);
$author->id = $id;

$authordao = new AuthorDao();

if (strlen($firstName) < 1 or strlen($firstName) > 21 or strlen($lastName) < 2 or strlen($lastName) > 22) {
    header("Location: author-add.php?name_error&firstname=$firstName&lastname=$lastName&grade=$grade");
    die();
}

if (isset($_POST['deleteButton'])) {
    $authordao->deleteAuthorById($id);
    header("Location: author-list.php?deleted");
    die();
}

if (isset($_POST['submitButton'])) {
    if ($_POST['submitButton'] === 'Uuenda') {
        $authordao->editAuthor($author);
        header("Location: author-list.php?updated");
        die();
    }
    if ($_POST['submitButton'] === 'Salvesta') {
        $authordao->saveAuthor($author);
        header("Location: author-list.php?saved");
        die();
    }
}
?>