<?php

require_once 'connection.php';

class AuthorDao
{


    function saveAuthor(Author $author) {
        $conn = getConnection();

        $stmt = $conn->prepare(
            'INSERT INTO authors (firstName, lastName, authorgrade) VALUES (:firstname, :lastname, :grade);');

        $stmt->bindValue(':firstname', htmlspecialchars($author->firstName, ENT_QUOTES));
        $stmt->bindValue(':lastname', htmlspecialchars($author->lastName, ENT_QUOTES));
        $stmt->bindValue(':grade', $author->grade);

        $stmt->execute();

        return $conn->lastInsertId();
    }

    function editAuthor(Author $author) {
        $conn = getConnection();
        $stmt = $conn->prepare(
            'UPDATE authors set 
                 firstName = :firstname,
                 lastName = :lastname,
                 authorgrade = :grade
                 WHERE id=:id');
        $stmt->bindValue(':firstname', htmlspecialchars($author->firstName, ENT_QUOTES));
        $stmt->bindValue(':lastname', htmlspecialchars($author->lastName, ENT_QUOTES));
        $stmt->bindValue(':grade', $author->grade);
        $stmt->bindValue(':id', $author->id);
        $stmt->execute();
        return $stmt;
    }


    function deleteAuthorById(string $id) {

        $conn = getConnection();
        $stmt = $conn->prepare(
            'DELETE FROM authors WHERE id=:id');
        $stmt->bindValue(':id', $id);
        $stmt->execute();

    }

    function getAuthors() {
        $conn = getConnection();

        $stmt= $conn->prepare('SELECT * FROM authors;');
        $stmt->execute();

        $ret_array = [];
        foreach ($stmt as $author) {
            $ret_array[] = $author;
        }
        return $ret_array;
    }

    function getAuthorById($id) : array {

        $conn = getConnection();

        $stmt = $conn->prepare('SELECT * FROM authors WHERE authors.id = :id');
        $stmt->bindValue(':id', $id);
        $stmt->execute();
        $return_array = [];


        foreach ($stmt as $row) {
            $return_array['id'] = $row[0];
            $return_array['firstName'] = $row['firstName'];
            $return_array['lastName'] = $row['lastName'];
            $return_array['grade'] = $row['authorgrade'];


        } return $return_array;

    }

}