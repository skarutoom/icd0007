<?php


class Book {

    public string $id ='';
    public string $title;
    public bool $isRead;
    public string $grade;
    public string $author1;
    public string $author2;

    public function __construct(string $title, string $grade, bool $isRead, string $author1, string $author2) {

        $this->title = $title;
        $this->isRead = $isRead;
        $this->grade = $grade;
        $this->author1 = $author1;
        $this->author2 = $author2;
    }

    public function __toString() : string {
        return sprintf('Id: %s, Title: %s, Read: %s, Author1: %s, Auhtor2: %s' . PHP_EOL,
            $this->id, $this->title,$this->grade , $this->isRead, $this->author1, $this->author2);
    }
}