<?php

require_once 'connection.php';

class BookDao
{
    function saveBook(Book $book) {
        $conn = getConnection();
        $stmt = $conn->prepare(
            'INSERT INTO books (title, grade, isRead) VALUES (:title, :grade, :isRead);');

        $stmt->bindValue(':title', htmlspecialchars($book->title, ENT_QUOTES));
        $stmt->bindValue(':grade', $book->grade);
        $stmt->bindValue(':isRead', intval($book->isRead));

        $stmt->execute();
        $book->id = $conn->lastInsertId();

        if (isset($book->author1)) {
            $this->bindBookAuthor($book->id, $book->author1);
        }
        if (isset($book->author2)) {
            $this->bindBookAuthor($book->id, $book->author2);
        }

        return $book->id;
    }

    function editBook(Book $book) {
        $conn = getConnection();
        $stmt = $conn->prepare(
            'UPDATE books set 
                 title = :title,
                 grade = :grade,
                 isRead = :isRead
                 WHERE id=:id;');
        $stmt->bindValue(':title', htmlspecialchars($book->title, ENT_QUOTES));
        $stmt->bindValue(':grade', $book->grade);
        $stmt->bindValue(':isRead', $book->isRead);
        $stmt->bindValue(':id', $book->id);
        $stmt->execute();

        $this->updateBookAuthor($book->id);

        if (isset($book->author1)) {
            $this->bindBookAuthor($book->id, $book->author1);
        }
        if (isset($book->author2)) {
            $this->bindBookAuthor($book->id, $book->author2);
        }
    }

    function deleteBookById($id) {
        $conn = getConnection();
        $stmt = $conn->prepare(
            'DELETE FROM books WHERE id=:id;');
        $stmt->bindValue(':id', $id);
        $stmt->execute();
    }

    function bindBookAuthor($bookId, $authorId) {
        $conn = getConnection();
        $stmt = $conn->prepare(
            'INSERT INTO books_authors (bookId, authorId) VALUE (:bookid, :authorid);');
        $stmt->bindValue(':bookid', intval($bookId));
        $stmt->bindValue(':authorid',intval($authorId));
        $stmt->execute();
    }

    function updateBookAuthor($bookId) {
        $conn = getConnection();
        $stmt = $conn->prepare(
            'DELETE from books_authors WHERE bookId = :bookid');
        $stmt->bindValue(':bookid', intval($bookId));
        $stmt->execute();
    }

    function editBookAuthor($bookId, $authorId) {
        $conn = getConnection();
    }

    function getBooks() {
        $conn = getConnection();

        $stmt = $conn->prepare('SELECT * FROM books LEFT JOIN books_authors ON books_authors.bookId = books.id LEFT JOIN authors ON books_authors.authorId = authors.id');

        $stmt->execute();
        $ret_array = [];
        foreach ($stmt as $row) {
            //print "-----1-----" . PHP_EOL;
            $bookId = $row['bookId'];
            $title = $row['title'];
            $author = $row['firstName'] . ' ' . $row['lastName'];
            $grade = $row['grade'];
            $isRead = $row['isRead'];
            if (isset($ret_array[$bookId])) {

                $ret_array[$bookId][3] = $author;
                $ret_array[$bookId][4] = $grade;
                $ret_array[$bookId][5] = $isRead;

            } else {
                $ret_array[$bookId] = [$bookId, $title, $author, ' ', $grade, $isRead];
            }
        } return $ret_array;
    }

    function getBookByID($id) : array
    {

        $conn = getConnection();

        $stmt = $conn->prepare('SELECT * FROM books 
            LEFT JOIN books_authors ON books_authors.bookId = books.id 
            LEFT JOIN authors ON books_authors.authorId = authors.id
            WHERE books.id = :id');
        $stmt->bindValue(':id', $id);
        $stmt->execute();

        $return_array = [];
        foreach ($stmt as $data) {
            $return_array['id'] = $data[0];
            $return_array['title'] = $data['title'];
            $return_array['grade'] = $data['grade'];
            $return_array['isread'] = $data['isRead'];
            $return_array['authors'][] = $data['id'];
        } return $return_array;
    }
}