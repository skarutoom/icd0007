<?php

require_once('BookDao.php');
require_once('AuthorDao.php');
require_once('Book.php');
require_once('Author.php');

$bookdao = new BookDao();
$authordao = new AuthorDao();

if (isset($_GET['query'])) {
    $query = $_GET['query'];
}

$cmd = 'returnAll';
if (isset($_GET['cmd'])) {
    $cmd = $_GET['cmd'];
}



if ($cmd === 'book-edit') {
    require_once "book-edit.php";
} else if ($cmd === 'book-add-submit') {
    require 'book-add-submit.php';
} else if ($cmd === 'returnAll') {
    require_once "book-list.php";
} else if ($cmd === 'book-list') {
    require_once "book-list.php";
} else if ($cmd === "book-form") {
    require_once 'book-add.php';
} else if ($cmd === 'author-list') {
    require_once "author-list.php";
    }


else {
    http_response_code(400);
}

