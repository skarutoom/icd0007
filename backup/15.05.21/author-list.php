<?php

$message = $_POST['query'] ?? '';

include_once __DIR__ . '/AuthorDao.php';
include_once 'tpl.php';

$dao = new AuthorDao();
$stmt = $dao->getAuthors();

$data = [
        'message' => $message,
        'authors' => $stmt,
        'template' => 'author-list.html'

];
print renderTemplate('tpl/main.html', $data);