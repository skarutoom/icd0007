<?php
$author_id = $_GET['id'] ?? '';

include_once __DIR__ . '/AuthorDao.php';
$dao = new AuthorDao();

$data = $dao->getAuthorById($author_id);

?>

<!DOCTYPE html>
<html lang="et">
<head>
    <meta charset="UTF-8">
    <title>Lisa Autor</title>
    <link href="styles.css" rel="stylesheet">
</head>
<body id="author-form-page">
<nav>
    <table>
        <tr>
            <td>
                <a href="index.php" id="book-list-link">Raamatud</a> |
                <a href="book-add.php" id="book-form-link">Lisa raamat</a> |
                <a href="author-list.php" id="author-list-link">Autorid</a> |
                <a href="author-add.php" id="author-form-link">Lisa autor</a>
            </td>
        </tr>
    </table>
</nav>
<?php
$return_m = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";

if (strpos($return_m, "name_error") == true) {
    print('<div id="error-block">Eesnimi peab olema 1 kuni 21 tähemärki ja perekonnanimi peab olema 2 kuni 22 tähemärki!</div>');
}
?>
<br>
<br>
<form method="post" action="add-author.php">
    <input name="id" type="hidden" value="<?php echo $data['id']?>">
    <table>
        <tr>
            <td>
                <label for="firstName">Eesnimi:</label>
            </td>
            <td>
                <input type="text"
                       id="firstName"
                       name="firstName"
                       value="<?php echo $data['firstName']?>"
                >
            </td>
        </tr>
        <tr>
            <td>
                <label for="lastName">Perekonnanimi:</label>
            </td>
            <td>
                <input type="text"
                       id="lastName"
                       name="lastName"
                       value = "<?php echo $data['lastName']?>"
                >
            </td>
        </tr>
        <tr>
            <td>
                <label>Hinne:</label>
            </td>
            <td>
                <?php foreach (range(1, 5) as $grade): ?>

                    <input type="radio"
                           name="grade"
                        <?= strval($grade) === $data["grade"] ? 'checked' : ''; ?>
                           value="<?= $grade ?>" />
                    <?= $grade ?>

                <?php endforeach; ?>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <input type="submit" id="deleteButton" name="deleteButton" value="Kustuta">
                <input type="submit" id="submitButton" name="submitButton" value="Uuenda">
            </td>
        </tr>
    </table>
</form>
<br>
<br>
<br>
<br>
<footer>
    ICD0007 Harjutus
</footer>
</body>
</html>