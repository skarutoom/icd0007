<?php
require_once 'connection.php';
function getBooks() {
    $conn = getConnection();

    $stmt = $conn->prepare('SELECT * FROM books LEFT JOIN books_authors ON books_authors.bookId = books.id LEFT JOIN authors ON books_authors.authorId = authors.id');

    $stmt->execute();
    $ret_array = [];
    foreach ($stmt as $row) {
        $bookId = $row['bookId'];
        $title = $row['title'];
        $author = $row['firstName'] . ' ' . $row['lastName'];
        $grade = $row['grade'];
        $isRead = $row['isRead'];
        if (isset($ret_array[$bookId])) {

            $ret_array[$bookId][3] = $author;
            $ret_array[$bookId][4] = $grade;
            $ret_array[$bookId][5] = $isRead;

        } else {
            $ret_array[$bookId] = [$bookId, $title, $author, ' ', $grade, $isRead];
        }
    } return $ret_array;
}

var_dump(getBooks());