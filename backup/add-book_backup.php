<?php

const DATA_FILE = __DIR__ . '/books.txt';

const ID_FILE = __DIR__ . '/book_id.txt';

$title = $_POST['title'] ?? '';
$grade = $_POST['grade'] ?? '';
$temp_isRead = $_POST['isRead'] ?? '';
$id = $_POST['id'] ?? '';

if ($temp_isRead === 'Yes') {
    $isRead = 'Yes';
} else {
    $isRead = 'No';
}

function saveBook(Book $book) : string {

    if ($book->id) {
        deletePostById($book->id);
    } else {
        $book->id = getNewId();
    }

    file_put_contents(DATA_FILE, FILE_APPEND);

    //return $post->id;
}

$line = urlencode(getNewId()) . ';' . urlencode($title) . ';' . urlencode($grade) . ';' . urlencode($isRead) . PHP_EOL;


file_put_contents(DATA_FILE, $line, FILE_APPEND);

if ($_POST['submitButton'] === 'Save') {
    header("Location: index.php");
}

function getNewId() : string {
    $contents = file_get_contents(ID_FILE);

    $id = intval($contents);

    file_put_contents(ID_FILE, $id + 1);

    return strval($id);
}

?>