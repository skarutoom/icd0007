<?php
$book_id = $_GET['id'] ?? '';
$title = $_GET['title'] ?? '';
$grade = $_GET['grade'] ?? '';
$actualGrade = $_GET['grade'] ?? '3';
$isRead = $_GET['isread'] ?? '';
?>

<!DOCTYPE html>
<html lang="et">
<head>
    <meta charset="UTF-8">
    <title>Lisa Raamat</title>
    <link href="styles.css" rel="stylesheet">
</head>
<body id="book-form-page">
<nav>
    <table>
        <tr>
            <td>
                <a href="index.php" id="book-list-link">Raamatud</a> |
                <a href="book-add.php" id="book-form-link">Lisa raamat</a> |
                <a href="author-list.php" id="author-list-link">Autorid</a> |
                <a href="author-add.html" id="author-form-link">Lisa autor</a>
            </td>
        </tr>
    </table>
</nav>
<br>
<br>
<br>
<form method="post" action="add-book.php">
    <table>
        <tr>
            <td>
                <label for="title">Pealkiri:</label>
            </td>
            <td>
                <input type="text"
                       id="title"
                       name="title"
                       minlength="3" maxlength="23"
                       value="<?= $title ?>"

                >
            </td>
        </tr>
        <tr>
            <td>
                <label for="author1">Autor 1:</label>
            </td>
            <td>
                <select id="author1">
                    <option value="auth1">Ed Tittel</option>
                    <option value="auth2">Stephen J. James</option>
                    <option value="auth3">J. M. Bedell</option>
                </select></td>
        </tr>
        <tr>
            <td>
                <label for="author2">Autor 2:</label>
            </td>
            <td>
                <select id="author2">
                <option value="auth1">Ed Tittel</option>
                <option value="auth2">Stephen J. James</option>
                <option value="auth3">J. M. Bedell</option>
            </select>
            </td>
        </tr>
        <tr>
            <td>
                <label>Hinne:</label>
            </td>
            <td>
                <?php foreach (range(1, 5) as $grade): ?>

                    <input type="radio"
                           name="grade"
                        <?= strval($grade) === $actualGrade ? 'checked' : ''; ?>
                           value="<?= $grade ?>" />
                    <?= $grade ?>

                <?php endforeach; ?>
            </td>
        </tr>
        <tr>
            <td>
                <label for="isRead">Loetud:</label>
            </td>
            <td>
                <input type="checkbox"
                       id="isRead"
                       name="isRead"
                       value="Yes"
                <?php if ($isRead === 'Yes') {
                    print 'checked';
                }?>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <input type="submit" id="submitButton" name="submitButton" value="Save">
            </td>
        </tr>
    </table>
</form>
<br>
<br>
<br>
<br>
<footer>
    ICD0007 Harjutus
</footer>
</body>
</html>