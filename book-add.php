<?php

include_once 'tpl.php';
include_once __DIR__ . '/AuthorDao.php';

$pageid = 'book-form-page';

$authordao = new AuthorDao();
$authors = $authordao->getAuthors();

$data = [
    'pageid' => $pageid,
    "authors" => $authors,
    "template" => 'book-add.html'
];

print renderTemplate('tpl/main.html', $data);
