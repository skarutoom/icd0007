<?php


function getAllBooks() : array
{

    $lines = file(DATA_FILE);

    $result = [];
    foreach ($lines as $line) {
        [$id, $title, $grade, $isRead] = explode(';', trim($line));

        $book = new Book(urldecode($title), urldecode($grade), urlencode($isRead));

        $book->id = $id;

        $result[] = $book;
    }

    return $result;
}

function getBookAsLine($book): string {
    return urlencode($book->id)
        . ';' . urlencode($book->title)
        . ';' . urlencode($book->grade)
        . ';' . urlencode($book->isRead) . PHP_EOL;
}

?>
