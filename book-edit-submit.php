<?php

include_once __DIR__ . '/BookDao.php';
include_once __DIR__ . '/Book.php';
include_once __DIR__ . '/AuthorDao.php';
include_once 'tpl.php';


$bookdao = new BookDao();


$submitButton = $_POST['submitButton'] ?? '';
$book_id = $_POST['id'] ?? '';
$title = $_POST['title'] ?? '';
$grade = $_POST['grade'] ?? '';
$isReadTemp = $_POST['isRead'] ?? '';
$author1 = $_POST['author1'] ?? '';
$author2 = $_POST['author2'] ?? '';

$isRead = false;
if ($isReadTemp === 'on') {
    $isRead = true;
} else {
    $isRead = false;
}

$book = new Book($title,$grade,$isRead,$author1,$author2);
$book->id = $book_id;

$pageid = 'book-form-page';
$pageid1 = 'book-list-page';

if (isset($_POST['deleteButton'])) {
    $bookdao->deleteBookById($book_id);
    header("Location: ?cmd=book-list&message=deleted");
    die();
}

if ($submitButton == 'Uuenda') {
    if (strlen($title) < 3 or strlen($title) > 23 ) {
        $authordao = new AuthorDao();
        $authors = $authordao->getAuthors();
        $message = "Pealkiri peab olema 3-23 tähemärki";
        $data = [
            'pageid' => $pageid,
            'id' => $book_id,
            'title' => $title,
            'author1' => $author1,
            'author2' => $author2,
            'grade' => $grade,
            'isread' => $isRead,
            'error' => $message,
            "authors" => $authors,
            'template' => 'book-edit.html'
        ];
        print renderTemplate('tpl/main.html', $data);
    } else {
        $bookdao->editBook($book);
        header("Location: ?cmd=book-list&message=updated");
        die();
    }
}

