<?php

include_once __DIR__ . '/BookDao.php';
include_once 'tpl.php';

$mes = '';
$message = '';

if (isset($_GET['message'])) {
    $mes = $_GET['message'];
}

if($mes === 'saved') {
    $message = "Salvestatud";
}
if($mes === 'updated') {
    $message = "Uuendatud";
}
if($mes === 'deleted') {
    $message = "Kustutatud";
}

$dao = new BookDao();
$books = $dao->getBooks();
$pageid = 'book-list-page';

$data = [
    'pageid' => $pageid,
    'books' => $books,
    'message' => $message,
    'template' => 'book-list.html'
];
print renderTemplate('tpl/main.html', $data);
