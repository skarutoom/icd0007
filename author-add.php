<?php
include_once 'tpl.php';
$pageid = 'author-form-page';

$data = [
    'pageid' => $pageid,
    'template' => 'author-add.html'
];
print renderTemplate('tpl/main.html', $data);