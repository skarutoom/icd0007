<?php

require_once 'connection.php';

class AuthorDao
{


    function saveAuthor($author) {
        $conn = getConnection();

        $stmt = $conn->prepare(
            'INSERT INTO authors (firstName, lastName, authorgrade) VALUES (:firstname, :lastname, :grade);');

        $stmt->bindValue(':firstname', htmlspecialchars($author->firstName, ENT_QUOTES));
        $stmt->bindValue(':lastname', htmlspecialchars($author->lastName, ENT_QUOTES));
        $stmt->bindValue(':grade', $author->grade);

        $stmt->execute();

        return $conn->lastInsertId();
    }

    function editAuthor($author) {
        $conn = getConnection();
        $stmt = $conn->prepare(
            'UPDATE authors set 
                 firstName = :firstname,
                 lastName = :lastname,
                 authorgrade = :grade
                 WHERE id=:id');
        $stmt->bindValue(':firstname', $author->firstName);
        $stmt->bindValue(':lastname', $author->lastName);
        $stmt->bindValue(':grade', $author->grade);
        $stmt->bindValue(':id', $author->id);
        $stmt->execute();
    }


    function deleteAuthorById(string $id) {

        $conn = getConnection();
        $stmt = $conn->prepare(
            'DELETE FROM authors WHERE id=:id');
        $stmt->bindValue(':id', $id);
        $stmt->execute();

    }

    function getAuthors() {
        $conn = getConnection();

        $stmt= $conn->prepare('SELECT * FROM authors;');
        $stmt->execute();

        $ret_array = [];
        foreach ($stmt as $row) {
            $author_id = $row['id'];
            $firstName = htmlspecialchars_decode($row['firstName']);
            $lastName = htmlspecialchars_decode($row['lastName']);
            $grade = $row['authorgrade'];

            $ret_array[$author_id] = [$author_id, $firstName, $lastName, $grade];

        }
        return $ret_array;
    }

    function getAuthorById($id) : array {

        $conn = getConnection();

        $stmt = $conn->prepare('SELECT * FROM authors WHERE authors.id = :id');
        $stmt->bindValue(':id', $id);
        $stmt->execute();
        $return_array = [];


        foreach ($stmt as $row) {
            $return_array['id'] = $row[0];
            $return_array['firstName'] = $row['firstName'];
            $return_array['lastName'] = $row['lastName'];
            $return_array['grade'] = $row['authorgrade'];


        } return $return_array;

    }

}