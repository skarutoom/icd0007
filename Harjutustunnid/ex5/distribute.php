<?php

//$sets = distributeToSets([1, 2, 1]);

//var_dump($sets);

function distributeToSets(array $input) : array {

    $sets = [];

    foreach ($input as $number) {

        if (isset($sets[$number])) {
            $sets[$number][] = $number;
        } else {
            $sets[$number] = [$number];
        }
    }
    return $sets;
}
