<?php

include_once __DIR__ . '/Post.php';

const DATA_FILE = __DIR__ . '/data/posts.txt';

function getAllPosts() : array {

    $lines = file(DATA_FILE);
    $postsList = [];
    foreach ($lines as $line) {
        [$title, $text] = explode(';', trim($line));

         $postsList[] = new Post(urldecode($title), urldecode($text));
    }
    print $postsList;
    return $postsList;
}

function savePost(Post $post) : void {

    $line = urlencode($post->title) . ';' . urlencode($post->text) . PHP_EOL;

    file_put_contents(DATA_FILE, $line, FILE_APPEND);
}

function printPosts(array $posts) {
    foreach ($posts as $post) {
        print $post . PHP_EOL;
    }
}