<?php

foreach (range(1,15) as $number) {

    if ($number % 3 === 0 && $number % 5 === 0) {
        print "fizzbuzz" . PHP_EOL;
    } else if ($number % 3 === 0) {
        print "fizz" . PHP_EOL;
    }  else if ($number % 5 === 0) {
        print "buzz" . PHP_EOL;
    } else {
        print $number . PHP_EOL;
    }
}
