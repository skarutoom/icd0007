<?php

$numbers = [1, 2, 5, 6, 2, 11, 2, 7];

function getOddNumbers($list) {
    $oddNumbers = [];
    foreach ($list as $element) {
        if ($element % 2 === 1) {
            $oddNumbers[] = $element;
        }
    }
    return $oddNumbers;
}
