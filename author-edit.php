<?php
include_once __DIR__ . '/AuthorDao.php';
include_once 'tpl.php';

$id = $_GET['id'];

$authordao = new AuthorDao();

$author = $authordao->getAuthorById($id);
$pageid = 'author-form-page';

$data = [
    'pageid' => $pageid,
    'firstName' => $author['firstName'],
    'lastName' => $author['lastName'],
    'grade' => $author['grade'],
    'id' => $id,
    'template' => 'author-edit.html'
];
print renderTemplate('tpl/main.html', $data);